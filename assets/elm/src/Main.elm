module Main exposing (main)

import Browser


main : Program () () ()
main = Browser.application
  { init = \ _ _ _ -> ( (), Cmd.none)
  , view = \ _ ->
    { title = "Blog"
    , body = []
    }
  , update = \ _ _ -> ( (), Cmd.none)
  , subscriptions = \ _ -> Sub.none
  , onUrlRequest = \ _ -> ()
  , onUrlChange = \ _ -> ()
  }
