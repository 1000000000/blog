defmodule Blog.Game.State do

  @enforce_keys [:supply_deck, :players, :supply]
  defstruct [
    :supply_deck,
    :supply,
    :players,
    turn: 0,
    turn_stage: :playing,
    gameover: false,
    ghost_writer: false
  ]

  alias Blog.Game.State, as: GameState
  alias Blog.Game.Player

  defp check_turn(%{players: players, turn: turn}, player) do
    if rem(turn, length players) == player do
      :ok
    else
      {:error, "You can only do that on your turn!"}
    end
  end

  defp check_stage(%{turn_stage: stage}, stage) do
    :ok
  end

  defp check_stage(_state, stage) do
    {:error, "You can only do that during the #{stage} stage!"}
  end

  def request_ghost_writer(state, player) do
    with :ok <- check_turn(state, player),
         :ok <- check_stage(state, :playing) do
      if state.ghost_writer do
        {:error, "You have already requested a ghost writer!"}
      else
        {:ok, %{state | ghost_writer: true}}
      end
    end
  end

  def new(num_players, supply_deck, starting_deck, starting_pool) when num_players > 0 do
    chunk_size = 10 - length(starting_deck)
    starting_decks =
      cond do
        chunk_size == 0 -> List.duplicate(starting_deck, num_players)
        chunk_size > 0 and num_players * chunk_size <= length(starting_pool) ->
            starting_pool
            |> Enum.take(num_players*chunk_size)
            |> Enum.shuffle
            |> Enum.chunk_every(chunk_size)
            |> Enum.map(&(&1 ++ starting_deck))
      end
    {starting_supply, finished_deck} =
      supply_deck
      |> Enum.shuffle
      |> Enum.take(7)
    %GameState{
      supply_deck: finished_deck,
      supply: starting_supply,
      players: Enum.map(starting_decks, &Player.new/1)
    }
  end
end
