defmodule Blog.Game.Card do
  @enforce_keys [:letter, :cost]
  defstruct [
    :letter,
    :cost,
    genre: nil,
    timeless: false,
    basic_benefits: %{},
    genre_benefits: %{},
    immediate_prestige: 0
  ]

  def load_cards(file) do
    # initialize atoms for card benefits
    _ = {
      :coin,
      :prestige,
      :trash_from_discard,
      :double_adjacent_card,
      :look_top_three,
      :uncover_adjacent_wild,
      :jail_offer_row_card,
      :wild_card_value,
      :coin_or_prestige,
      :ink_or_remover,
      :others_lose_ink_or_remover,
      :trash_this_card,
      :gain_per_genre
    }
    %{starting_deck: starting_deck, starting_pool: starting_pool, supply_deck: supply_deck} =
      file
      |> File.read!
      |> Jason.decode!(keys: :atoms!, strings: :copy)
    {
      Enum.map(supply_deck, &struct(Card, &1)),
      Enum.map(starting_deck, &struct(Card, &1)),
      Enum.map(starting_pool, &struct(Card, &1))
    }
  end
end
