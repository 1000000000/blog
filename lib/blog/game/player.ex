defmodule Blog.Game.Player do
  alias Blog.Game.Player

  defmodule PlayedCard do
    @enforce_keys [:card, :type]
    defstruct [:card, :type]
  end

  @enforce_keys [:deck, :hand]
  defstruct [
    :deck,
    :hand,
    jail: nil,
    discard: [],
    timeless_classics: %{},
    timeless_counter: 0,
    played: [],
    score: 0,
    ink: 0,
    remover: 0
  ]

  defp shuffle_discard(player = %{deck: []}) do
    {unshuffled_deck, tmp_player} = Map.get_and_update!(player, :discard, &{&1, []})
    %{tmp_player | deck: Enum.shuffle(unshuffled_deck)}
  end

  # Returns {cards, player}
  defp draw_at_most(player, n) when n >= 0 do
    {drawn, new_player} = Map.get_and_update!(player, :deck, &Enum.split(&1, n))
    undershot = n - length drawn
    if undershot > 0 do
      {more_drawn, done_player} =
        new_player
        |> shuffle_discard
        |> Map.get_and_update!(:deck, &Enum.split(&1, undershot))
      {drawn ++ more_drawn, done_player}
    else
      {drawn, new_player}
    end
  end

  def ink(player = %{ink: ink}) when ink > 0 do
    {maybe_card, new_player} = draw_at_most(player, 1)
    case maybe_card do
      [] ->
        {:error, "No cards left to ink!"}
      [card] ->
        finished_player =
          new_player
          |> Map.update!(:ink, &(&1 - 1))
          |> Map.update!(:played, &[%PlayedCard{card: card, type: :ink} | &1])
        {:ok, finished_player}
    end
  end

  def ink(_), do: {:error, "No ink left!"}

  defp update_played(player = %{played: played}, card_index, f) do
    if card_index >= 0 and card_index < length(played) do
      {former, [card | latter]} = Enum.split(played, card_index)
      with {:ok, updated_card} <- f.(card) do
        {:ok, %{player | played: former ++ [updated_card | latter]}}
      end
    else
      {:error, "Card index out of bounds: #{card_index}"}
    end
  end

  def remover(player = %{remover: remover}, card_index) when remover > 0 do
    remove_ink = fn
      card = %{type: :ink} -> %{card | type: :hand}
      _card -> {:error, "Card does not have ink!"}
    end
    update_played(%{ player | remover: remover - 1}, card_index, remove_ink)
  end

  def remover(_,_), do: {:error, "No remover left!"}

  def change_wild(player, card_index, new_char) do
    change_char = fn
      %{type: {:wild, ^new_char}} -> :noop
      card = %{type: {:wild, _old_char}} -> {:ok, %{card | type: {:wild, new_char}}}
      %{type: non_wild} -> {:error, "Cannot change the letter of a non-wild card: #{non_wild}"}
    end
    update_played(player, card_index, change_char)
  end

  def make_wild(player, card_index, wild_char) do
    hand_to_wild = fn
      card = %{type: :hand} -> {:ok, %{card | type: {:wild, wild_char}}}
      %{type: non_hand} -> {:error, "Cannot make #{non_hand} cards wild only regular cards from hand!"}
    end
    update_played(player, card_index, hand_to_wild)
  end

  defp remove_card(player, field, index) do
    list = Map.fetch!(player, field)
    if index >= 0 and index < length(list) do
      {card, new_list} = List.pop_at(list, index)
      {:ok, card, Map.replace!(player, field, new_list)}
    else
      {:error, "Index of target in #{field} out of bounds: #{index}"}
    end
  end

  defp add_card(player, field, index, card) do
    list = Map.fetch!(player, field)
    if index >= 0 and index <= length(list) do
      new_list = List.insert_at(list, index, card)
      {:ok, Map.replace!(player, field, new_list)}
    else
      {:error, "Index of card in #{field} out of bounds: #{index}"}
    end
  end

  defp move_card(player, source_field, dest_field, source_index, dest_index, f \\ &Function.identity/1)

  defp move_card(_player, field, field, index, index, _f), do: :noop

  defp move_card(player, source_field, dest_field, source_index, dest_index, f) do
    with {:ok, card, tmp_player} <- remove_card(player, source_field, source_index),
         {:ok, updated_card} <- f.(card) do
      add_card(tmp_player, dest_field, dest_index, updated_card)
    end
  end

  def play_card(player, hand_index, play_index) do
    move_card(player, :hand, :played, hand_index, play_index, &{:ok, %PlayedCard{card: &1, type: :hand}})
  end

  def play_wild(player, hand_index, play_index, wild_char) do
    move_card(player, :hand, :played, hand_index, play_index, &{:ok, %PlayedCard{card: &1, type: {:wild, wild_char}}})
  end

  def check_timeless(player, timeless_index) do
    if Map.has_key?(player.timeless_classics, timeless_index) do
      :ok
    else
      {:error, "You do not have a timeless classic with the index #{timeless_index}!"}
    end
  end

  def play_own_timeless(player, timeless_index, play_index) do
    with :ok <- check_timeless(player, timeless_index) do
      if %PlayedCard{card: timeless_index, type: :own_timeless} in player.played do
        add_card(player, :played, play_index, %PlayedCard{card: timeless_index, type: :own_timeless})
      else
        {:error, "Already played own timeless_classic with index #{timeless_index}!"}
      end
    end
  end

  def own_timeless_used(player, timeless_index) do
    with :ok <- check_timeless(player, timeless_index) do
      {card, tmp_player1} = Map.get_and_update!(player, :timeless_classics, &Map.pop!(&1, timeless_index))
      final_player =
        tmp_player1
        |> Map.update!(:played, &List.delete(&1, %PlayedCard{card: timeless_index, type: :own_timeless}))
        |> Map.update!(:discard, &[card | &1])
      {:ok, final_player}
    end
  end

  def move_played_card(player, old_index, new_index) do
    move_card(player, :played, :played, old_index, new_index)
  end

  def move_hand_card(player, old_index, new_index) do
    move_card(player, :hand, :hand, old_index, new_index)
  end

  def pickup_card(player, play_index, hand_index) do
    check_card = fn
      %{card: card, type: :hand} -> {:ok, card}
      %{card: card, type: {:wild, _char}} -> {:ok, card}
      %{type: non_hand_wild} -> {:error, "Can only pick up regular hand cards or wild cards, not #{non_hand_wild}!"}
    end
    move_card(player, :played, :hand, play_index, hand_index, check_card)
  end

  def discard_hand(player) do
    {hand, handless_player} = Map.get_and_update!(player, :hand, &{&1, []})
    Map.update!(handless_player, :discard, & hand ++ &1)
  end

  def add_purchases_end_turn(player = %{hand: []}, bought_cards, bought_ink) do
    immediate_prestige = Enum.reduce(bought_cards, 0, &(&1.immediate_prestige + &2))
    {played_cards, tmp_player} = Map.get_and_update!(player, :played, &{&1, []})
    inked = Enum.count(played_cards, &(&1.type == :ink))
    card_grouper = fn
      %{card: %{timeless: true}} -> :new_timeless
      %{type: {:wild, _}} -> :deck
      %{type: :hand} -> :deck
      %{type: :ink} -> :deck
      %{type: timeless_classic} -> timeless_classic
    end
    %{new_timeless: new_timeless, deck: deck, opponent_timeless: opponent_timeless} =
      Enum.group_by(played_cards, card_grouper, &(&1.card))
    new_timeless_map =
      new_timeless
      |> Enum.with_index(player.timeless_counter)
      |> Map.new(fn {card, key} -> {key, card} end)
    error_on_duplicate_key = fn
      k, _c1, _c2 -> raise "Attempted to add a timeless classic with already existing index #{k}!"
    end
    {new_hand, almost_done_player} =
      tmp_player
      |> Map.update!(:ink, &(bought_ink + &1))
      |> Map.update!(:score, &(immediate_prestige + &1))
      |> Map.update!(:discard, &(deck ++ bought_cards ++ &1))
      |> Map.update!(:timeless_classics, &Map.merge(&1, new_timeless_map, error_on_duplicate_key))
      |> Map.update!(:timeless_counter, &(&1 + length(new_timeless)))
      |> draw_at_most(5)
    {inked, opponent_timeless, Map.replace!(almost_done_player, :hand, new_hand)}
  end

  def writers_block(player) do
    {played, tmp_player1} = Map.get_and_update!(player, :played, &{&1, []})
    played_cards =
      for %{card: card, type: type} when type not in [:opponent_timeless, :own_timeless] <- played, do: card
    {hand_cards, tmp_player2} = Map.get_and_update!(tmp_player1, :hand, &{&1, []})
    {new_hand, tmp_player3} =
      tmp_player2
      |> Map.update!(:discard, &(played_cards ++ hand_cards ++ &1))
      |> draw_at_most(5)
    Map.replace!(tmp_player3, :hand, new_hand)
  end

  def new(starting_deck) do
    {hand, deck} =
      starting_deck
      |> Enum.shuffle
      |> Enum.split(5)
    %Player{deck: deck, hand: hand}
  end

end
