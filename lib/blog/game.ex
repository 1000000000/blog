defmodule Blog.Game do
  use GenServer
  alias Blog.Game.State, as: GameState

  @impl true
  def init(num_players) do
    {:ok, GameState.new(num_players, [], [], [])}
  end
end
